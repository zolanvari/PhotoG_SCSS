# Data Description:

## ORGANISING THE DATA CAPTURE

The data was recorded in the early hours of Saturday 15th of
July 2017, maximising the chance of good weather conditions
with low human traffic on the ground. The preparation of
this project required the following to get permission for this
recording:

* A short support email from a TCD academic staff indicating the usefulness of the data for research and education.

* An email from Intel-Movidius to the relevant authorities in Trinity College providing license and public liability insurance information for their drone.


On the day of the recording, several research staff volunteered
to help monitor the process on the ground. Intel-Movidius
contributed in providing staff to fly the drone capturing the
800+ images of the campus and in generating the point cloud
and mesh files also distributed with the dataset.

* https://creativecommons.org/licenses/by/4.0/

* https://scss.tcd.ie/iman.zolanvari/PC/TCD.html


## CALL FOR ACADEMIC PARTICIPATION
Intel-Movidius is interested in contributing to the creation of 3D datasets to be used for research and education purposes.

If you are an academic in the Republic of Ireland & Northern Ireland and you want to capture a 3D model of your campus for research and education, please contact jonathan.byrne@movidius.com. The process is straight forward as indicated in above. 

## Acknowledgement
This research is supported by Intel-Movidius, Trinity College
Dublin and several funding bodies including the ADAPT
Centre for Digital Content Technology funded under the
SFI Research Centres Programme (Grant 13/RC/2106) cofunded
under the European Regional Development Fund,
and European projects FP7-PEOPLE-2013-IAPP GRAISearch
(612334) and H2020-ICT-2016-1 (732204).


### Please cite as:
J. Byrne, J. Connelly, J. Su, V. Krylov, M. Bourke, D. Moloney & R. Dahyot, Trinity College Dublin Drone Survey Dataset, 2017, School of Computer Science & Statistics, Trinity College Dublin, 2017

