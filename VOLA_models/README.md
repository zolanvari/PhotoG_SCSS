
![Voxel model of TCD Campus](/VOLA_models/TCD_vola.png)

## Citation:

Please cite the original authors if you use the VOLA code:

@author Jonathan Byrne & Anton Shmatov
@copyright 2018 Intel Ltd (see LICENSE file).
